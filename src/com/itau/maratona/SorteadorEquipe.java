package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;
//import java.util.Random;

public class SorteadorEquipe {
	public static ArrayList<Equipe> sortear(List<Aluno> alunos) {
		ArrayList<Equipe> equipes = new ArrayList<>();
		
		int contador = 1;
		
		while(alunos.size() > 0) {
			Equipe equipe = new Equipe();
			
			int vezes;
//			int vezes = alunos.size() >= 3 ? 3 : alunos.size();
			
			if(alunos.size() >= 3) {
				vezes = 3;
			}else {
				vezes = alunos.size();
			}
			
			for(int i = 0; i < vezes; i++) {
				int sorteio = (int) Math.floor(Math.random() * alunos.size());
				
				//Poderia utilizar também o random
				//		Random random = new Random();
				//      return random.nextInt(Tamanho);
				
				Aluno aluno = alunos.remove(sorteio);
				
				equipe.alunos.add(aluno);
			}
			
			equipe.id = contador;
			equipes.add(equipe);
			
			contador++;
		}
		
		return equipes;
	}
}
